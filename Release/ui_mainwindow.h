/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QAction *action2_Units;
    QAction *actionExit_2;
    QAction *actionUma_Unidade;
    QAction *action_Sobre;
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *mainTab;
    QListWidget *cadList;
    QLabel *label;
    QPushButton *insCad;
    QGroupBox *groupBox;
    QLabel *label_2;
    QLabel *cdName;
    QLabel *label_4;
    QLabel *profName;
    QLabel *label_3;
    QLabel *medMin;
    QLabel *label_5;
    QLabel *cCreds;
    QLabel *aulas;
    QLabel *label_7;
    QLabel *unLabel;
    QWidget *calendar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuNova_Cadeira;
    QMenu *menuAjuda;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(720, 432);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        action2_Units = new QAction(MainWindow);
        action2_Units->setObjectName(QStringLiteral("action2_Units"));
        actionExit_2 = new QAction(MainWindow);
        actionExit_2->setObjectName(QStringLiteral("actionExit_2"));
        actionUma_Unidade = new QAction(MainWindow);
        actionUma_Unidade->setObjectName(QStringLiteral("actionUma_Unidade"));
        action_Sobre = new QAction(MainWindow);
        action_Sobre->setObjectName(QStringLiteral("action_Sobre"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 721, 371));
        mainTab = new QWidget();
        mainTab->setObjectName(QStringLiteral("mainTab"));
        cadList = new QListWidget(mainTab);
        cadList->setObjectName(QStringLiteral("cadList"));
        cadList->setGeometry(QRect(10, 40, 256, 111));
        label = new QLabel(mainTab);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 71, 21));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        insCad = new QPushButton(mainTab);
        insCad->setObjectName(QStringLiteral("insCad"));
        insCad->setGeometry(QRect(170, 10, 91, 23));
        groupBox = new QGroupBox(mainTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(280, 30, 431, 121));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 30, 46, 13));
        cdName = new QLabel(groupBox);
        cdName->setObjectName(QStringLiteral("cdName"));
        cdName->setGeometry(QRect(110, 30, 120, 13));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 50, 71, 16));
        profName = new QLabel(groupBox);
        profName->setObjectName(QStringLiteral("profName"));
        profName->setGeometry(QRect(110, 50, 121, 16));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 70, 61, 16));
        medMin = new QLabel(groupBox);
        medMin->setObjectName(QStringLiteral("medMin"));
        medMin->setGeometry(QRect(110, 70, 21, 16));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(240, 30, 46, 13));
        cCreds = new QLabel(groupBox);
        cCreds->setObjectName(QStringLiteral("cCreds"));
        cCreds->setGeometry(QRect(300, 30, 46, 13));
        aulas = new QLabel(groupBox);
        aulas->setObjectName(QStringLiteral("aulas"));
        aulas->setGeometry(QRect(110, 90, 301, 16));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(240, 50, 51, 16));
        unLabel = new QLabel(groupBox);
        unLabel->setObjectName(QStringLiteral("unLabel"));
        unLabel->setGeometry(QRect(300, 50, 51, 16));
        tabWidget->addTab(mainTab, QString());
        calendar = new QWidget();
        calendar->setObjectName(QStringLiteral("calendar"));
        tabWidget->addTab(calendar, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 720, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuNova_Cadeira = new QMenu(menuFile);
        menuNova_Cadeira->setObjectName(QStringLiteral("menuNova_Cadeira"));
        menuAjuda = new QMenu(menuBar);
        menuAjuda->setObjectName(QStringLiteral("menuAjuda"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuAjuda->menuAction());
        menuFile->addAction(menuNova_Cadeira->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionExit_2);
        menuAjuda->addAction(action_Sobre);

        retranslateUi(MainWindow);
        QObject::connect(insCad, SIGNAL(clicked()), insCad, SLOT(showMenu()));
        QObject::connect(insCad, SIGNAL(clicked()), MainWindow, SLOT(OnClickEv()));
        QObject::connect(insCad, SIGNAL(clicked()), cadList, SLOT(scrollToTop()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        action2_Units->setText(QApplication::translate("MainWindow", "2 Units", 0));
        actionExit_2->setText(QApplication::translate("MainWindow", "&Sair", 0));
        actionUma_Unidade->setText(QApplication::translate("MainWindow", "Uma Unidade", 0));
        action_Sobre->setText(QApplication::translate("MainWindow", "&Sobre", 0));
        label->setText(QApplication::translate("MainWindow", "Cadeiras", 0));
        insCad->setText(QApplication::translate("MainWindow", "Inserir Cadeira", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Informa\303\247\303\265es da Cadeira", 0));
        label_2->setText(QApplication::translate("MainWindow", "Nome:", 0));
        cdName->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "Professor(a):", 0));
        profName->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "M\303\251dia M\303\255n.:", 0));
        medMin->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "Cr\303\251ditos:", 0));
        cCreds->setText(QString());
        aulas->setText(QString());
        label_7->setText(QApplication::translate("MainWindow", "Unidades:", 0));
        unLabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(mainTab), QApplication::translate("MainWindow", "Info", 0));
        tabWidget->setTabText(tabWidget->indexOf(calendar), QApplication::translate("MainWindow", "Calend\303\241rio", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "&Arquivo", 0));
        menuNova_Cadeira->setTitle(QApplication::translate("MainWindow", "Nova Cadeira", 0));
        menuAjuda->setTitle(QApplication::translate("MainWindow", "A&juda", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
