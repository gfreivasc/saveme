/********************************************************************************
** Form generated from reading UI file 'createcad.ui'
**
** Created by: Qt User Interface Compiler version 5.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATECAD_H
#define UI_CREATECAD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_CreateCad
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *nome;
    QLineEdit *prof;
    QLineEdit *Creds;
    QLineEdit *unit;
    QLineEdit *medMin;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;

    void setupUi(QDialog *CreateCad)
    {
        if (CreateCad->objectName().isEmpty())
            CreateCad->setObjectName(QStringLiteral("CreateCad"));
        CreateCad->resize(400, 300);
        buttonBox = new QDialogButtonBox(CreateCad);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        nome = new QLineEdit(CreateCad);
        nome->setObjectName(QStringLiteral("nome"));
        nome->setGeometry(QRect(90, 30, 113, 20));
        prof = new QLineEdit(CreateCad);
        prof->setObjectName(QStringLiteral("prof"));
        prof->setGeometry(QRect(90, 60, 113, 20));
        Creds = new QLineEdit(CreateCad);
        Creds->setObjectName(QStringLiteral("Creds"));
        Creds->setGeometry(QRect(90, 90, 51, 20));
        unit = new QLineEdit(CreateCad);
        unit->setObjectName(QStringLiteral("unit"));
        unit->setGeometry(QRect(90, 120, 51, 20));
        medMin = new QLineEdit(CreateCad);
        medMin->setObjectName(QStringLiteral("medMin"));
        medMin->setGeometry(QRect(90, 150, 51, 20));
        label = new QLabel(CreateCad);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 30, 51, 16));
        label_2 = new QLabel(CreateCad);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 60, 51, 16));
        label_3 = new QLabel(CreateCad);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 90, 61, 16));
        label_4 = new QLabel(CreateCad);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 120, 51, 16));
        label_5 = new QLabel(CreateCad);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 150, 61, 16));

        retranslateUi(CreateCad);
        QObject::connect(buttonBox, SIGNAL(accepted()), CreateCad, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CreateCad, SLOT(reject()));

        QMetaObject::connectSlotsByName(CreateCad);
    } // setupUi

    void retranslateUi(QDialog *CreateCad)
    {
        CreateCad->setWindowTitle(QApplication::translate("CreateCad", "Dialog", 0));
        label->setText(QApplication::translate("CreateCad", "Nome:", 0));
        label_2->setText(QApplication::translate("CreateCad", "Prof.:", 0));
        label_3->setText(QApplication::translate("CreateCad", "Cr\303\251ditos:", 0));
        label_4->setText(QApplication::translate("CreateCad", "Unidades:", 0));
        label_5->setText(QApplication::translate("CreateCad", "M\303\251dia Min.:", 0));
    } // retranslateUi

};

namespace Ui {
    class CreateCad: public Ui_CreateCad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATECAD_H
