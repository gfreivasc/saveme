#ifndef CADEIRA_H
#define CADEIRA_H

#include <QString>

class Cadeira
{
private:
    QString nome;
    QString prof;
    int creditos;
    double medMin;
    int unidades;
    int provasFeitas;
    double *notas;
public:
    Cadeira(QString n, QString p, int c, double m, int u);
    ~Cadeira();
    void addNota(double n);
    double Media();
    double Need();
    double MParcial();
    QString gNome() { return this->nome; }
    QString gProf() { return this->prof; }
    int gCreds() { return this->creditos; }
    double gMed() { return this->medMin; }
    int gUnit() { return this->unidades; }
};

extern Cadeira *c[64];
extern int cCount;

#endif // CADEIRA_H
