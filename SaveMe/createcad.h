#ifndef CREATECAD_H
#define CREATECAD_H

#include <QDialog>

namespace Ui {
class CreateCad;
}

class CreateCad : public QDialog
{
    Q_OBJECT
    
public:
    explicit CreateCad(QWidget *parent = 0);
    ~CreateCad();
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::CreateCad *ui;
};

extern bool diag_ac;

#endif // CREATECAD_H
