#include "cadeira.h"
#include "mainwindow.h"
#include "createcad.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->insCad, SIGNAL(clicked()), this, SLOT(newItem()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_insCad_clicked()
{
    CreateCad cc;
    cc.exec();
}

void MainWindow::newItem()
{
    if(diag_ac) ui->cadList->addItem(c[cCount - 1]->gNome());
    diag_ac = false;
}

void MainWindow::on_cadList_itemClicked(QListWidgetItem *item)
{
    ui->cdName->setText(item->text());
    ui->cCreds->setText(QString::number(c[ui->cadList->currentRow()]->gCreds()));
    ui->profName->setText(c[ui->cadList->currentRow()]->gProf());
    ui->medMin->setText(QString::number(c[ui->cadList->currentRow()]->gMed(),'g',1));
    ui->unLabel->setText(QString::number(c[ui->cadList->currentRow()]->gUnit()));
}
