#include "createcad.h"
#include "cadeira.h"
#include "ui_createcad.h"

bool diag_ac = false;

CreateCad::CreateCad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateCad)
{
    ui->setupUi(this);
}

CreateCad::~CreateCad()
{
    delete ui;
}

void CreateCad::on_buttonBox_accepted()
{
    bool ok;
    c[cCount++] = new Cadeira(ui->nome->text(), ui->prof->text(), ui->Creds->text().toInt(&ok, 10), ui->medMin->text().toDouble(&ok), ui->unit->text().toInt(&ok, 10));
    diag_ac = true;
}
