#-------------------------------------------------
#
# Project created by QtCreator 2013-12-19T15:35:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SaveMe
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cadeira.cpp \
    createcad.cpp

HEADERS  += mainwindow.h \
    ../cadeira.h \
    cadeira.h \
    createcad.h

FORMS    += mainwindow.ui \
    createcad.ui
