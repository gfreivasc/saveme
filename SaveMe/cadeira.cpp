#include "cadeira.h"
#include <cstring>

Cadeira *c[64];
int cCount = 0;

Cadeira::Cadeira(QString n, QString p, int c, double m, int u)
{
    this->nome = n;
    this->prof = p;
    this->creditos = c;
    this->medMin = m;
    this->unidades = u;
    this->notas = new double[u];
    for(int i = 0; i < u; ++i) this->notas[i] = 0;
    this->provasFeitas = 0;
}

Cadeira::~Cadeira()
{
    delete this->notas;
}

void Cadeira::addNota(double n)
{
    this->notas[this->provasFeitas++] = n;
}

double Cadeira::Media()
{
    double soma = 0;

    for(int i = 0; i < this->unidades; ++i)
    {
        soma += this->notas[i];
    }

    soma /= this->unidades;
    return soma;
}

double Cadeira::Need()
{
    double aux = 0;
    int i = 0;
    int u = this->unidades;

    for(; i < u; ++i)
    {
        if(!this->notas[i]) break;
        aux += this->notas[i];
    }

    i = u - i;

    if(i) aux = (u*this->medMin - aux)/i;

    return aux;
}

double Cadeira::MParcial()
{
    double soma;

    for(int i = 0; i < this->provasFeitas; ++i)
    {
        soma += this->notas[i];
    }

    soma /= this->provasFeitas;

    return soma;
}
